﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuePhotoManager_2_0.AppUtil
{
    /// <summary>
    /// 应用配置
    /// </summary>
    /// <remarks>2016-6-24 SoChishun Added.</remarks>
    class AppConf
    {
        public static string version = "2.0.0.0";
        public static string name = "照片重命名大师";
    }
}
