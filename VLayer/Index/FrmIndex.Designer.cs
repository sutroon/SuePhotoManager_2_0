﻿namespace SuePhotoManager_2_0.VLayer.Index
{
    partial class FrmIndex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_path_src = new System.Windows.Forms.TextBox();
            this.btn_open_src = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_name_format = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chk_has_subdir = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chk_is_copy = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_path_dest = new System.Windows.Forms.TextBox();
            this.btn_open_dest = new System.Windows.Forms.Button();
            this.btn_run = new System.Windows.Forms.Button();
            this.pbar_main = new System.Windows.Forms.ProgressBar();
            this.lbl_counter = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chk_keep_struct = new System.Windows.Forms.CheckBox();
            this.panel_keep_struct = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.lnk_about = new System.Windows.Forms.LinkLabel();
            this.label9 = new System.Windows.Forms.Label();
            this.chk_resize = new System.Windows.Forms.CheckBox();
            this.panel_size = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_width = new System.Windows.Forms.TextBox();
            this.tb_height = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.tb_keywords = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tb_ext_classes = new System.Windows.Forms.TextBox();
            this.btn_go_src = new System.Windows.Forms.Button();
            this.btn_go_dst = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.chk_is_classify = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lnk_help = new System.Windows.Forms.LinkLabel();
            this.panel_keep_struct.SuspendLayout();
            this.panel_size.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "选择照片目录：";
            // 
            // tb_path_src
            // 
            this.tb_path_src.Location = new System.Drawing.Point(15, 38);
            this.tb_path_src.Name = "tb_path_src";
            this.tb_path_src.Size = new System.Drawing.Size(456, 21);
            this.tb_path_src.TabIndex = 1;
            // 
            // btn_open_src
            // 
            this.btn_open_src.Location = new System.Drawing.Point(478, 35);
            this.btn_open_src.Name = "btn_open_src";
            this.btn_open_src.Size = new System.Drawing.Size(56, 23);
            this.btn_open_src.TabIndex = 2;
            this.btn_open_src.Text = "选择(&S)";
            this.btn_open_src.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "文件名格式：";
            // 
            // tb_name_format
            // 
            this.tb_name_format.Location = new System.Drawing.Point(86, 70);
            this.tb_name_format.Name = "tb_name_format";
            this.tb_name_format.Size = new System.Drawing.Size(167, 21);
            this.tb_name_format.TabIndex = 4;
            this.tb_name_format.Text = "\'[EXT]_\'yyyyMMdd_hhmmss";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 248);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "包含子目录：";
            // 
            // chk_has_subdir
            // 
            this.chk_has_subdir.AutoSize = true;
            this.chk_has_subdir.Checked = true;
            this.chk_has_subdir.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_has_subdir.Location = new System.Drawing.Point(85, 248);
            this.chk_has_subdir.Name = "chk_has_subdir";
            this.chk_has_subdir.Size = new System.Drawing.Size(15, 14);
            this.chk_has_subdir.TabIndex = 6;
            this.chk_has_subdir.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "保留源文件：";
            // 
            // chk_is_copy
            // 
            this.chk_is_copy.AutoSize = true;
            this.chk_is_copy.Checked = true;
            this.chk_is_copy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_is_copy.Location = new System.Drawing.Point(86, 103);
            this.chk_is_copy.Name = "chk_is_copy";
            this.chk_is_copy.Size = new System.Drawing.Size(15, 14);
            this.chk_is_copy.TabIndex = 8;
            this.chk_is_copy.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 296);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "选择保存目录：";
            // 
            // tb_path_dest
            // 
            this.tb_path_dest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_path_dest.Location = new System.Drawing.Point(15, 311);
            this.tb_path_dest.Name = "tb_path_dest";
            this.tb_path_dest.Size = new System.Drawing.Size(454, 21);
            this.tb_path_dest.TabIndex = 10;
            // 
            // btn_open_dest
            // 
            this.btn_open_dest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_open_dest.Location = new System.Drawing.Point(476, 309);
            this.btn_open_dest.Name = "btn_open_dest";
            this.btn_open_dest.Size = new System.Drawing.Size(58, 23);
            this.btn_open_dest.TabIndex = 11;
            this.btn_open_dest.Text = "选择(&D)";
            this.btn_open_dest.UseVisualStyleBackColor = true;
            // 
            // btn_run
            // 
            this.btn_run.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_run.Location = new System.Drawing.Point(443, 338);
            this.btn_run.Name = "btn_run";
            this.btn_run.Size = new System.Drawing.Size(104, 39);
            this.btn_run.TabIndex = 12;
            this.btn_run.Text = "开始(&R)";
            this.btn_run.UseVisualStyleBackColor = true;
            // 
            // pbar_main
            // 
            this.pbar_main.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pbar_main.Location = new System.Drawing.Point(0, 383);
            this.pbar_main.Name = "pbar_main";
            this.pbar_main.Size = new System.Drawing.Size(555, 11);
            this.pbar_main.TabIndex = 13;
            // 
            // lbl_counter
            // 
            this.lbl_counter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_counter.AutoSize = true;
            this.lbl_counter.Location = new System.Drawing.Point(15, 368);
            this.lbl_counter.Name = "lbl_counter";
            this.lbl_counter.Size = new System.Drawing.Size(35, 12);
            this.lbl_counter.TabIndex = 14;
            this.lbl_counter.Text = "(0/0)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.DarkViolet;
            this.label6.Location = new System.Drawing.Point(260, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(281, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "不转义的内容用单引号包含起来!变量[EXT]表示文件";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.DarkViolet;
            this.label7.Location = new System.Drawing.Point(260, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(287, 12);
            this.label7.TabIndex = 16;
            this.label7.Text = "后缀名称:[EXT]:保持默认大小写;[EXT:U]:强制大写;";
            // 
            // chk_keep_struct
            // 
            this.chk_keep_struct.AutoSize = true;
            this.chk_keep_struct.Location = new System.Drawing.Point(101, 2);
            this.chk_keep_struct.Name = "chk_keep_struct";
            this.chk_keep_struct.Size = new System.Drawing.Size(15, 14);
            this.chk_keep_struct.TabIndex = 0;
            this.chk_keep_struct.UseVisualStyleBackColor = true;
            // 
            // panel_keep_struct
            // 
            this.panel_keep_struct.Controls.Add(this.chk_keep_struct);
            this.panel_keep_struct.Controls.Add(this.label8);
            this.panel_keep_struct.Location = new System.Drawing.Point(118, 247);
            this.panel_keep_struct.Name = "panel_keep_struct";
            this.panel_keep_struct.Size = new System.Drawing.Size(122, 16);
            this.panel_keep_struct.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "保存子目录结构：";
            // 
            // lnk_about
            // 
            this.lnk_about.AutoSize = true;
            this.lnk_about.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnk_about.Location = new System.Drawing.Point(493, 9);
            this.lnk_about.Name = "lnk_about";
            this.lnk_about.Size = new System.Drawing.Size(41, 12);
            this.lnk_about.TabIndex = 18;
            this.lnk_about.TabStop = true;
            this.lnk_about.Text = "[关于]";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 270);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 19;
            this.label9.Text = "照片瘦身：";
            // 
            // chk_resize
            // 
            this.chk_resize.AutoSize = true;
            this.chk_resize.Location = new System.Drawing.Point(74, 269);
            this.chk_resize.Name = "chk_resize";
            this.chk_resize.Size = new System.Drawing.Size(15, 14);
            this.chk_resize.TabIndex = 20;
            this.chk_resize.UseVisualStyleBackColor = true;
            // 
            // panel_size
            // 
            this.panel_size.Controls.Add(this.label12);
            this.panel_size.Controls.Add(this.tb_width);
            this.panel_size.Controls.Add(this.tb_height);
            this.panel_size.Controls.Add(this.label11);
            this.panel_size.Controls.Add(this.label10);
            this.panel_size.Location = new System.Drawing.Point(95, 263);
            this.panel_size.Name = "panel_size";
            this.panel_size.Size = new System.Drawing.Size(438, 27);
            this.panel_size.TabIndex = 21;
            this.panel_size.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.DarkViolet;
            this.label12.Location = new System.Drawing.Point(164, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(269, 12);
            this.label12.TabIndex = 8;
            this.label12.Text = "值\"0\"表示自适应尺寸,宽度和高度至少一个不为0!";
            // 
            // tb_width
            // 
            this.tb_width.Location = new System.Drawing.Point(28, 3);
            this.tb_width.Name = "tb_width";
            this.tb_width.Size = new System.Drawing.Size(46, 21);
            this.tb_width.TabIndex = 5;
            this.tb_width.Text = "1024";
            // 
            // tb_height
            // 
            this.tb_height.Location = new System.Drawing.Point(108, 3);
            this.tb_height.Name = "tb_height";
            this.tb_height.Size = new System.Drawing.Size(47, 21);
            this.tb_height.TabIndex = 7;
            this.tb_height.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(85, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 6;
            this.label11.Text = "高：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 4;
            this.label10.Text = "宽：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.DarkViolet;
            this.label13.Location = new System.Drawing.Point(260, 102);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 12);
            this.label13.TabIndex = 22;
            this.label13.Text = "[EXT:L]:强制小写.";
            // 
            // btn_cancel
            // 
            this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_cancel.Location = new System.Drawing.Point(374, 338);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(63, 39);
            this.btn_cancel.TabIndex = 23;
            this.btn_cancel.Text = "取消(&C)";
            this.btn_cancel.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 126);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 12);
            this.label14.TabIndex = 24;
            this.label14.Text = "不更名关键词：";
            // 
            // tb_keywords
            // 
            this.tb_keywords.Location = new System.Drawing.Point(101, 123);
            this.tb_keywords.Name = "tb_keywords";
            this.tb_keywords.Size = new System.Drawing.Size(440, 21);
            this.tb_keywords.TabIndex = 25;
            this.tb_keywords.Text = "IMG_|VIDEO_|_";
            this.toolTip1.SetToolTip(this.tb_keywords, "多个关键词以英文分号隔开(*:全部不更名)");
            // 
            // tb_ext_classes
            // 
            this.tb_ext_classes.Location = new System.Drawing.Point(101, 170);
            this.tb_ext_classes.Multiline = true;
            this.tb_ext_classes.Name = "tb_ext_classes";
            this.tb_ext_classes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_ext_classes.Size = new System.Drawing.Size(440, 70);
            this.tb_ext_classes.TabIndex = 27;
            this.tb_ext_classes.Text = "IMG:jpg|jpeg|png|gif|tiff|bmp\r\nVIDEO:mp4|mpeg|mov|flv";
            this.toolTip1.SetToolTip(this.tb_ext_classes, "多个关键词以英文分号隔开");
            // 
            // btn_go_src
            // 
            this.btn_go_src.Location = new System.Drawing.Point(533, 35);
            this.btn_go_src.Name = "btn_go_src";
            this.btn_go_src.Size = new System.Drawing.Size(14, 23);
            this.btn_go_src.TabIndex = 31;
            this.btn_go_src.Text = "...";
            this.toolTip1.SetToolTip(this.btn_go_src, "打开目录");
            this.btn_go_src.UseVisualStyleBackColor = true;
            // 
            // btn_go_dst
            // 
            this.btn_go_dst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_go_dst.Location = new System.Drawing.Point(533, 309);
            this.btn_go_dst.Name = "btn_go_dst";
            this.btn_go_dst.Size = new System.Drawing.Size(14, 23);
            this.btn_go_dst.TabIndex = 32;
            this.btn_go_dst.Text = "...";
            this.toolTip1.SetToolTip(this.btn_go_dst, "打开目录");
            this.btn_go_dst.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 150);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 12);
            this.label15.TabIndex = 26;
            this.label15.Text = "扩展名归类：";
            // 
            // chk_is_classify
            // 
            this.chk_is_classify.AutoSize = true;
            this.chk_is_classify.Checked = true;
            this.chk_is_classify.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_is_classify.Location = new System.Drawing.Point(101, 150);
            this.chk_is_classify.Name = "chk_is_classify";
            this.chk_is_classify.Size = new System.Drawing.Size(15, 14);
            this.chk_is_classify.TabIndex = 28;
            this.chk_is_classify.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.DarkViolet;
            this.label16.Location = new System.Drawing.Point(122, 150);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(215, 12);
            this.label16.TabIndex = 29;
            this.label16.Text = "把扩展名对应的类别作为[EXT]变量的值";
            // 
            // lnk_help
            // 
            this.lnk_help.AutoSize = true;
            this.lnk_help.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lnk_help.Location = new System.Drawing.Point(454, 9);
            this.lnk_help.Name = "lnk_help";
            this.lnk_help.Size = new System.Drawing.Size(41, 12);
            this.lnk_help.TabIndex = 30;
            this.lnk_help.TabStop = true;
            this.lnk_help.Text = "[帮助]";
            // 
            // FrmIndex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 394);
            this.Controls.Add(this.btn_go_dst);
            this.Controls.Add(this.btn_go_src);
            this.Controls.Add(this.lnk_help);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.chk_is_classify);
            this.Controls.Add(this.tb_ext_classes);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.tb_keywords);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.chk_resize);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lnk_about);
            this.Controls.Add(this.panel_keep_struct);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbl_counter);
            this.Controls.Add(this.pbar_main);
            this.Controls.Add(this.btn_run);
            this.Controls.Add(this.btn_open_dest);
            this.Controls.Add(this.tb_path_dest);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chk_is_copy);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chk_has_subdir);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_name_format);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_open_src);
            this.Controls.Add(this.tb_path_src);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel_size);
            this.Name = "FrmIndex";
            this.Text = "FrmIndex";
            this.Load += new System.EventHandler(this.FrmIndex_Load);
            this.panel_keep_struct.ResumeLayout(false);
            this.panel_keep_struct.PerformLayout();
            this.panel_size.ResumeLayout(false);
            this.panel_size.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_path_src;
        private System.Windows.Forms.Button btn_open_src;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_name_format;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chk_has_subdir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chk_is_copy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_path_dest;
        private System.Windows.Forms.Button btn_open_dest;
        private System.Windows.Forms.Button btn_run;
        private System.Windows.Forms.ProgressBar pbar_main;
        private System.Windows.Forms.Label lbl_counter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chk_keep_struct;
        private System.Windows.Forms.Panel panel_keep_struct;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.LinkLabel lnk_about;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chk_resize;
        private System.Windows.Forms.Panel panel_size;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_width;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_height;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb_keywords;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tb_ext_classes;
        private System.Windows.Forms.CheckBox chk_is_classify;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.LinkLabel lnk_help;
        private System.Windows.Forms.Button btn_go_src;
        private System.Windows.Forms.Button btn_go_dst;

    }
}