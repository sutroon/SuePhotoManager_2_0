﻿using SuePhotoManager_2_0.AppUtil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuePhotoManager_2_0.VLayer.About
{
    public partial class FrmAbout : Form
    {
        public FrmAbout()
        {
            InitializeComponent();
        }

        private void FrmAbout_Load(object sender, EventArgs e)
        {
            StringBuilder sbld = new StringBuilder();
            sbld.AppendFormat("名称：{0}{1}", AppConf.name, Environment.NewLine);
            sbld.AppendFormat("版本：{0}{1}", AppConf.version, Environment.NewLine);
            sbld.AppendLine("作者：SoChishun");
            sbld.AppendLine("邮箱：14507247@qq.com");
            sbld.AppendLine("=======================================================");
            sbld.AppendLine(AppConf.name+"是一款用于批量重命名文件或者照片的实用小工具，支持照片瘦身，需要.Net Framework 4.0支持。");
            sbld.AppendLine("对产品有什么意见或建议，请致信作者，不甚感激!");
            rtb_content.Text = sbld.ToString();
        }
    }
}
