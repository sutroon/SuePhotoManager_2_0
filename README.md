#SuePhotoManager_2_0
照片重命名管家 2.0<br />
<br />
**特性：**<br />
1. 提取照片的创建时间或修改时间作为文件名一部分，支持文件名格式设置<br />
2. 支持指定输出目录<br />
3. 支持缩略图输出<br />
<br />
**截图：**<br />
主界面<br />
![主界面.png](ProjectResourceLibrary/snapshot/1.png)<br /><br />
帮助文档<br />
![帮助文档.png](ProjectResourceLibrary/snapshot/2.png)<br /><br />
操作确认框<br />
![操作确认框.png](ProjectResourceLibrary/snapshot/3.png)<br /><br />
操作完成<br />
![操作完成.png](ProjectResourceLibrary/snapshot/4.png)<br /><br />
未操作前文件名<br />
![未操作前文件名.png](ProjectResourceLibrary/snapshot/5.png)<br /><br />
操作后文件名<br />
![操作后文件名.png](ProjectResourceLibrary/snapshot/6.png)<br /><br />
操作日志<br />
![操作日志.png](ProjectResourceLibrary/snapshot/7.png)<br /><br />
项目主页：http://git.oschina.net/sutroon/SuePhotoManager_2_0<br />